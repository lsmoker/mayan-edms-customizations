from mayan.apps.appearance.classes import Icon


icon_most_viewed_document_list = Icon(
    driver_name='fontawesome', symbol='fire'
)

icon_name_search = Icon(
    driver_name='fontawesome', symbol='search'
)

icon_document_global_favorites_list = Icon(
    driver_name='fontawesome', symbol='sun'
)

icon_favorite_tags_list = Icon(
    driver_name='fontawesome', symbol='tags'
)

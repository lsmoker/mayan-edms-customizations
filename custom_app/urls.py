from django.conf.urls import url

from .views import (
    MostViewedDocumentListView, NameSearchView, NameSearchResultsView,
    DocumentGlobalFavoritesListView, FavoriteTagsListView, HomeView
)


urlpatterns = [
    url(
        regex=r'^documents/most_viewed/$', name='most_viewed_document_list',
        view=MostViewedDocumentListView.as_view()
    ),
    url(
        regex=r'^documents/name_search/$', name='name_search',
        view=NameSearchView.as_view()
    ),
    url(
        regex=r'^documents/name_search_results/$', name='name_search_results',
        view=NameSearchResultsView.as_view()
    ),
    url(
        regex=r'^documents/document_global_favorites_list/$', name='document_global_favorites_list',
        view=DocumentGlobalFavoritesListView.as_view()
    ),
    url(
        regex=r'^tags/favorite_tags_list/$', name='favorite_tags_list',
        view=FavoriteTagsListView.as_view()
    ),
    url(
        regex=r'^home/$', name='home',
        view=HomeView.as_view()
    ),
]

import logging

from django import forms

from mayan.apps.acls.models import AccessControlList
from mayan.apps.documents.models.document_models import Document
from mayan.apps.documents.permissions import permission_document_view

logger = logging.getLogger(name=__name__)


class NameSearchForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(NameSearchForm, self).__init__(*args, **kwargs)

        queryset = AccessControlList.objects.restrict_queryset(
            permission=permission_document_view,
            user=self.user,
            queryset=Document.valid.all()
        )

        self.document_name_search = forms.ModelMultipleChoiceField(
            queryset=queryset,
            required=False,
            widget=forms.SelectMultiple(attrs={
                'class': 'select2',
                'data-placeholder': 'Begin typing a document name here. Multiple names can be entered.',
                'data-allow-clear': 'true',
                'data-minimum-input-length': '3'
            })
        )

        self.fields['document_name_search'] = self.document_name_search

from django.apps import apps
from django.core.exceptions import PermissionDenied
from django.urls import reverse

from mayan.apps.documents.permissions import permission_document_file_download
from mayan.apps.navigation.html_widgets import SourceColumnWidget
from mayan.apps.views.html_widgets import ObjectLinkWidget


class DocumentDownloadSourceWidget(SourceColumnWidget):
    template_string = '<a target="_blank" href="{{ url }}">{{ label }}</a>'

    def get_extra_context(self):
        AccessControlList = apps.get_model(
            app_label='acls', model_name='AccessControlList'
        )

        document_file = self.value.file_latest

        try:
            AccessControlList.objects.check_access(
                obj=document_file,
                permissions=(permission_document_file_download,),
                user=self.request.user
            )
        except PermissionDenied:
            url = '#'
        else:
            url = reverse(
                viewname='documents:document_file_download',
                kwargs={'document_file_id': document_file.pk}
            )

        return {
            'label': 'Download', 'url': url
        }

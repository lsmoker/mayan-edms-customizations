import logging

from django.apps import apps
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.db.models import Count
from django.shortcuts import render

from mayan.apps.views.generics import (
    SimpleView, SingleObjectListView
)
from mayan.apps.common.views import HomeView as DefaultHomeView
from mayan.apps.documents.models.document_models import Document
from mayan.apps.documents.views.document_views import DocumentListView
from mayan.apps.dynamic_search.icons import icon_search_submit
from mayan.apps.metadata.models import MetadataType
from mayan.apps.tags.models import Tag
from mayan.apps.tags.views import TagListView

from .icons import (
    icon_most_viewed_document_list, icon_document_global_favorites_list,
    icon_favorite_tags_list
)
from .forms import NameSearchForm

logger = logging.getLogger(name=__name__)


class MostViewedDocumentListView(DocumentListView):
    def get_document_queryset(self):
        return Document.valid. \
            filter(target_actions__verb='documents.document_view').annotate(views=Count('target_actions')). \
            filter(views__gte=settings.MOST_VIEWED_DOCUMENT_MINIMUM_VIEWS).order_by('-views')

    def get_extra_context(self):
        context = super(MostViewedDocumentListView, self).get_extra_context()
        context.update(
            {
                'no_results_icon': icon_most_viewed_document_list,
                'no_results_text': _(
                    'Most viewed documents will be listed in this view. '
                ),
                'no_results_title': _('There are no most viewed documents.'),
                'title': _('Documents viewed at least %s times' % settings.MOST_VIEWED_DOCUMENT_MINIMUM_VIEWS),
            }
        )
        return context


class NameSearchView(SimpleView):
    template_name = 'appearance/generic_form.html'
    title = _('Name search')

    def get_extra_context(self):
        return {
            'form': self.get_form(),
            'form_action': reverse(
                viewname='custom:name_search_results',
            ),
            'submit_icon_class': icon_search_submit,
            'submit_label': _('Search'),
            'submit_method': 'GET',
            'title': _('Search for document by name'),
        }

    def get_form(self):
        return NameSearchForm(
            data=self.request.GET, user=self.request.user
        )


class NameSearchResultsView(SingleObjectListView):
    def get_extra_context(self):
        context = {
            'hide_object': True,
            'list_as_items': True,
            'title': _('Search results'),
        }

        return context

    def get_source_queryset(self):
        if self.request.GET:
            query_string = self.request.GET.copy()

            queryset = Document.valid.filter(id__in=query_string.getlist('document_name_search'))

            return queryset


class DocumentGlobalFavoritesListView(DocumentListView):
    def get_document_queryset(self):
        return Document.valid.filter(tags__pk=settings.TAG_ID_GLOBAL_FAVORITE)

    def get_extra_context(self):
        context = super(DocumentGlobalFavoritesListView, self).get_extra_context()
        context.update(
            {
                'no_results_icon': icon_document_global_favorites_list,
                'no_results_text': _(
                    'Global favorites will be listed in this view. '
                ),
                'no_results_title': _('There are no global favorites.'),
                'title': _('Global favorites'),
            }
        )
        return context


class FavoriteTagsListView(TagListView):
    def get_tag_queryset(self):
        fav_tag_ids = MetadataType.objects.get(name='favorite_tags')
        return Tag.objects.filter(pk__in=fav_tag_ids.lookup.split(','))

    def get_extra_context(self):
        context = super(FavoriteTagsListView, self).get_extra_context()
        context.update(
            {
                'no_results_icon': icon_favorite_tags_list,
                'no_results_text': _(
                    'Favorite Tags will be listed in this view. '
                ),
                'no_results_title': _('There are no favorite tags.'),
                'title': _('Favorite Tags'),
            }
        )
        return context


class HomeNameSearchView(NameSearchView):
    template_name = 'appearance/generic_form_subtemplate.html'

    def get_extra_context(self):
        extra_context = super().get_extra_context() or {}
        extra_context.pop('submit_icon_class', None)
        extra_context.pop('title', None)
        return extra_context


class HomeView(DefaultHomeView):
    template_name = 'home.html'

    def get(self, request, *args, **kwargs):
        # document name search
        nsv_html = HomeNameSearchView.as_view()(request).render().content.decode("utf-8")
        self.extra_context['nsv_html'] = nsv_html

        return render(request, self.template_name, self.extra_context)


def csrf_failure(request, reason=''):
    from django.middleware.csrf import REASON_NO_CSRF_COOKIE, REASON_NO_REFERER
    response = render(
        request,
        "403_csrf.html",
        {
            "title": _("Permission Denied (custom error handler)"),
            "no_referer": reason == REASON_NO_REFERER,
            "no_cookie": reason == REASON_NO_CSRF_COOKIE,
            "reason_code": reason,
        },
        status=403,
    )
    # Avoid setting CSRF cookie on CSRF failure page, otherwise we end up creating
    # new session even when user might already have one (because browser did not
    # send the cookies with the CSRF request and Django doesn't see the session
    # cookie).
    response.csrf_cookie_set = True
    return response

import logging

from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from mayan.apps.common.apps import MayanAppConfig

from mayan.apps.authentication.links import link_password_change
from mayan.apps.common.menus import (
    menu_object, menu_user
)
from mayan.apps.documents.dashboard_widgets import (
    DashboardWidgetUserRecentlyAccessedDocuments,
    DashboardWidgetUserRecentlyCreatedDocuments,
    DashboardWidgetUserFavoriteDocuments
)
from mayan.apps.documents.menus import menu_documents
from mayan.apps.navigation.classes import SourceColumn
from mayan.apps.tags.menus import menu_tags
from mayan.apps.user_management.dashboards import dashboard_user

from .dashboard_widgets import (
    DashboardWidgetUserMostViewedDocuments,
    DashboardWidgetUserGlobalFavoriteDocuments,
    DashboardWidgetUserFavoriteTags,
    CustomDashboardWidgetUserRecentlyAccessedDocuments,
    CustomDashboardWidgetUserRecentlyCreatedDocuments,
    CustomDashboardWidgetUserFavoriteDocuments
)

from .html_widgets import DocumentDownloadSourceWidget

from .links import (
    link_document_global_favorites_list, link_most_viewed_documents_list,
    link_name_search, link_document_download_quick, link_favorite_tags_list
)

logger = logging.getLogger(name=__name__)


class CustomApp(MayanAppConfig):
    app_namespace = 'custom'
    app_url = 'custom_url'
    has_rest_api = False
    has_tests = False
    name = 'mayan.apps.Custom'
    verbose_name = _('Custom')

    def ready(self):
        super(CustomApp, self).ready()

        from mayan.apps.documents.models import Document, DocumentFile
        from mayan.apps.tags.models import Tag

        # change order of Comment SourceColumn in DocumentFile view
        source_columns = SourceColumn._registry.get(DocumentFile, ())
        for source_column in source_columns:
            if source_column.attribute == 'comment':
                source_column.order = -90

        # add SourceColumns for dashboard widgets
        SourceColumn(
            attribute='label', is_object_absolute_url=True,
            is_identifier=True, is_sortable=True,
            name='label', source=Tag
        )

        SourceColumn(
            func=lambda context: context[
                'object'
            ].get_document_count(
                user=context['request'].user
            ), is_identifier=True, name='doc_count', source=Tag
        )

        SourceColumn(
            func=lambda context: context[
                'object'
            ].cabinets.first().get_full_path(),
            is_identifier=True, name='cab_label', source=Document
        )

        # add Download link for dashboard widgets on home page
        SourceColumn(
            label=_('Download'), source=Document,
            is_identifier=True, name='download_link',
            widget=DocumentDownloadSourceWidget
        )

        # add Timestamp for DocumentFile thumbnail view
        SourceColumn(
            attribute='timestamp', include_label=True, is_sortable=True,
            order=-10, source=DocumentFile
        )

        # Remove "Change password" link from user menu
        menu_user.unbind_links(
            links=(link_password_change,),
        )

        # Add "Quick Download" link
        menu_object.bind_links(
            links=(
                link_document_download_quick,
            ), sources=(Document,)
        )

        # Add "Global Favorites", "Most viewed", "Document name search" link
        menu_documents.bind_links(
            links=(
                link_document_global_favorites_list,
                link_most_viewed_documents_list,
                link_name_search,
            ),
            position=3,
        )

        # Add "Global Favorite Tags" link
        menu_tags.bind_links(
            links=(
                link_favorite_tags_list,
            ),
            position=1,
        )

        dashboard_user.add_widget(
            widget=DashboardWidgetUserMostViewedDocuments,
            order=2
        )

        dashboard_user.add_widget(
            widget=DashboardWidgetUserGlobalFavoriteDocuments,
            order=1
        )

        dashboard_user.add_widget(
            widget=DashboardWidgetUserFavoriteTags,
            order=0
        )

        dashboard_user.remove_widget(
            widget=DashboardWidgetUserRecentlyAccessedDocuments
        )

        dashboard_user.add_widget(
            widget=CustomDashboardWidgetUserRecentlyAccessedDocuments,
            order=4
        )

        dashboard_user.remove_widget(
            widget=DashboardWidgetUserRecentlyCreatedDocuments
        )

        dashboard_user.add_widget(
            widget=CustomDashboardWidgetUserRecentlyCreatedDocuments,
            order=3
        )

        dashboard_user.remove_widget(
            widget=DashboardWidgetUserFavoriteDocuments
        )

        dashboard_user.add_widget(
            widget=CustomDashboardWidgetUserFavoriteDocuments,
            order=5
        )

        Document._meta.get_field('label').verbose_name = 'Document name'

        from . import signals

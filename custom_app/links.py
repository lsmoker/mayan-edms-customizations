from django.utils.translation import ugettext_lazy as _

from mayan.apps.documents.icons import icon_document_file_download_quick
from mayan.apps.documents.permissions import permission_document_file_download
from mayan.apps.navigation.classes import Link
from mayan.apps.navigation.utils import factory_condition_queryset_access
from mayan.apps.tags.icons import icon_tag_list
from mayan.apps.tags.permissions import permission_tag_view

from .icons import (
    icon_most_viewed_document_list, icon_name_search,
    icon_document_global_favorites_list
)


link_most_viewed_documents_list = Link(
    icon=icon_most_viewed_document_list,
    text=_('Most viewed'),
    view='custom:most_viewed_document_list'
)

link_name_search = Link(
    icon=icon_name_search,
    text=_('Document name search'),
    view='custom:name_search'
)

link_document_global_favorites_list = Link(
    icon=icon_document_global_favorites_list,
    text=_('Global Favorites'),
    view='custom:document_global_favorites_list'
)

link_favorite_tags_list = Link(
    condition=factory_condition_queryset_access(
        app_label='tags', model_name='Tag',
        object_permission=permission_tag_view,
    ), icon=icon_tag_list,
    text=_('Global Favorite Tags'),
    view='custom:favorite_tags_list'
)

link_document_download_quick = Link(
    args='resolved_object.file_latest.pk', icon=icon_document_file_download_quick,
    permissions=(permission_document_file_download,),
    text=_('Quick download'), view='documents:document_file_download'
)

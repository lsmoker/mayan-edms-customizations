from django.apps import apps
from django.conf import settings
from django.db.models import Count
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from mayan.apps.dashboards.classes import DashboardWidgetList
from mayan.apps.documents.dashboard_widgets import (
    DashboardWidgetUserRecentlyAccessedDocuments,
    DashboardWidgetUserRecentlyCreatedDocuments,
    DashboardWidgetUserFavoriteDocuments
)
from mayan.apps.documents.permissions import permission_document_view
from mayan.apps.tags.permissions import permission_tag_view

from .links import (
    link_most_viewed_documents_list, link_document_global_favorites_list,
    link_favorite_tags_list
)


class DashboardWidgetUserMostViewedDocuments(DashboardWidgetList):
    columns = ('document_type', 'label', 'cab_label', 'download_link',)
    icon = link_most_viewed_documents_list.icon
    label = link_most_viewed_documents_list.text
    link = reverse_lazy(
        link_most_viewed_documents_list.view
    )
    object_list_length = 30

    def get_object_list(self):
        AccessControlList = apps.get_model(
            app_label='acls', model_name='AccessControlList'
        )
        Document = apps.get_model(
            app_label='documents', model_name='Document'
        )

        queryset = Document.valid. \
            filter(target_actions__verb='documents.document_view').annotate(views=Count('target_actions')). \
            filter(views__gte=settings.MOST_VIEWED_DOCUMENT_MINIMUM_VIEWS).order_by('-views')

        return AccessControlList.objects.restrict_queryset(
            permission=permission_document_view, user=self.request.user,
            queryset=queryset
        )


class DashboardWidgetUserGlobalFavoriteDocuments(DashboardWidgetList):
    columns = ('document_type', 'label', 'cab_label', 'download_link',)
    icon = link_document_global_favorites_list.icon
    label = link_document_global_favorites_list.text
    link = reverse_lazy(
        viewname=link_document_global_favorites_list.view
    )
    object_list_length = 30

    def get_object_list(self):
        AccessControlList = apps.get_model(
            app_label='acls', model_name='AccessControlList'
        )
        Document = apps.get_model(
            app_label='documents', model_name='Document'
        )

        queryset = Document.valid.filter(tags__pk=settings.TAG_ID_GLOBAL_FAVORITE)

        return AccessControlList.objects.restrict_queryset(
            permission=permission_document_view, user=self.request.user,
            queryset=queryset
        )


class DashboardWidgetUserFavoriteTags(DashboardWidgetList):
    columns = ('label', 'doc_count',)
    icon = link_favorite_tags_list.icon
    label = link_favorite_tags_list.text
    link = reverse_lazy(
        viewname=link_favorite_tags_list.view
    )

    def get_object_list(self):
        AccessControlList = apps.get_model(
            app_label='acls', model_name='AccessControlList'
        )
        Tag = apps.get_model(
            app_label='tags', model_name='Tag'
        )

        from mayan.apps.metadata.models import MetadataType
        fav_tag_ids = MetadataType.objects.get(name='favorite_tags')
        queryset = Tag.objects.filter(id__in=fav_tag_ids.lookup.split(','))

        return AccessControlList.objects.restrict_queryset(
            permission=permission_tag_view, user=self.request.user,
            queryset=queryset
        )


class CustomDashboardWidgetUserRecentlyAccessedDocuments(DashboardWidgetUserRecentlyAccessedDocuments):
    columns = ('datetime_accessed', 'label', 'cab_label', 'download_link',)


class CustomDashboardWidgetUserRecentlyCreatedDocuments(DashboardWidgetUserRecentlyCreatedDocuments):
    columns = ('datetime_created', 'label', 'cab_label', 'download_link',)


class CustomDashboardWidgetUserFavoriteDocuments(DashboardWidgetUserFavoriteDocuments):
    columns = ('datetime_added', 'label', 'cab_label', 'download_link',)

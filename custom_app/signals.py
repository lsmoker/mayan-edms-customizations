import logging

from django.db.models.signals import m2m_changed, post_save
from django.dispatch import receiver

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404

from mayan.apps.acls.models import AccessControlList
from mayan.apps.cabinets.models import Cabinet
from mayan.apps.documents.models.document_models import Document
from mayan.apps.events.models import Notification
from mayan.apps.permissions.classes import Permission

logger = logging.getLogger(name=__name__)


def add_cabinet_permissions_to_document(cabinet, documents_pk):
    # get acls for root cabinet
    cab_acls = AccessControlList.objects.filter(
        content_type=get_object_or_404(
            ContentType, app_label='cabinets',
            model='cabinet'
        ),
        object_id=cabinet.get_root().pk
    )

    # get acls from "default" document
    doc_acls = AccessControlList.objects.filter(
        content_type=get_object_or_404(
            ContentType, app_label='documents',
            model='document'
        ),
        object_id=settings.DOCID_FOR_PERMISSIONS
    )

    if doc_acls:
        doc_perms = {}
        # get doc permissions into a dict of lists
        for role_type in ['Editor', 'Reader']:
            try:
                perms_for_role = doc_acls.filter(role__label__endswith=role_type).get().permissions.all()
                doc_perms[role_type] = [Permission.get(
                                            pk='{}.{}'.format(permission.namespace, permission.name),
                                            class_only=True
                                        ) for permission in perms_for_role]
            except:
                pass

        # add permissions from cabinet roles
        for cab_acl in cab_acls:
            for doc_id in documents_pk:
                if doc_id != settings.DOCID_FOR_PERMISSIONS and doc_perms and cab_acl.role.label.split()[-1] in ['Editor', 'Reader']:
                    for doc_permission in doc_perms[cab_acl.role.label.split()[-1]]:
                        AccessControlList.objects.grant(
                            obj=Document.objects.get(pk=doc_id), permission=doc_permission, role=cab_acl.role
                        )


@receiver(m2m_changed, sender=Cabinet.documents.through, dispatch_uid='custom_handler_post_document_cabinet_add')
def handler_post_document_cabinet_add(sender, **kwargs):
    if settings.DOCID_FOR_PERMISSIONS and kwargs.get('action', None) == 'post_add' and isinstance(kwargs.get('instance'), Cabinet):
        add_cabinet_permissions_to_document(kwargs.get('instance'), kwargs.get('pk_set', set()))


@receiver(m2m_changed, sender=Cabinet.documents.through, dispatch_uid='custom_handler_post_document_cabinet_remove')
def handler_post_document_cabinet_remove(sender, **kwargs):
    if settings.DOCID_FOR_PERMISSIONS and kwargs.get('action', None) == 'post_remove':
        for doc_id in kwargs.get('pk_set', set()):
            # delete Editor/Reader permissions
            for role_type in ['Editor', 'Reader']:
                AccessControlList.objects.filter(
                    content_type=get_object_or_404(
                        ContentType, app_label='documents',
                        model='document'
                    ),
                    object_id=doc_id,
                    role__label__endswith=role_type
                ).delete()
            # add cabinet permissions back
            for cabinet in Document.objects.get(pk=doc_id).cabinets.all():
                add_cabinet_permissions_to_document(cabinet, (doc_id,))


@receiver(post_save, sender=Notification, dispatch_uid='custom_handler_post_notification_save')
def handler_post_notification_save(sender, instance, created, **kwargs):
    if created:
        pass

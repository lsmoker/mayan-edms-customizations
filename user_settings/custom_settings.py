import ldap

from django_auth_ldap.config import (
    LDAPSearch, LDAPSearchUnion, NestedActiveDirectoryGroupType
)

from mayan.settings.production import *

# needed to workaround a bug at https://gitlab.com/mayan-edms/mayan-edms/blob/master/mayan/apps/appearance/static/appearance/js/partial_navigation.js#L337
# https://gitlab.com/mayan-edms/mayan-edms/-/issues/500
middleware_list = list(MIDDLEWARE)
middleware_list.remove('django.middleware.csrf.CsrfViewMiddleware')

MIDDLEWARE = tuple(middleware_list) + ('django_remote_auth_ldap.middleware.RemoteUserMiddleware',)

DOCID_FOR_PERMISSIONS = 37

TAG_ID_GLOBAL_FAVORITE = 1

MOST_VIEWED_DOCUMENT_MINIMUM_VIEWS = 100

# Makes sure this works in Active Directory
ldap.set_option(ldap.OPT_REFERRALS, False)

# Turn off debug output, turn this off when everything is working as expected
ldap.set_option(ldap.OPT_DEBUG_LEVEL, 0)

# Default: True
AUTH_LDAP_ALWAYS_UPDATE_USER = True

AUTH_LDAP_START_TLS = False

AUTH_LDAP_BIND_DN = EMAIL_HOST_USER
AUTH_LDAP_BIND_PASSWORD = EMAIL_HOST_PASSWORD
AUTH_LDAP_SERVER_URI = 'ldap://example.org'

# Simple search
AUTH_LDAP_USER_SEARCH = LDAPSearchUnion(
    LDAPSearch(
        'ou=Users,dc=example,dc=org', ldap.SCOPE_SUBTREE,
        '(samaccountname=%(user)s)'
    ),
    LDAPSearch(
        'ou=Protected Users,dc=example,dc=org',
        ldap.SCOPE_SUBTREE, '(samaccountname=%(user)s)'
    ),
)

# User attributes to map from LDAP to Mayan's user model.
AUTH_LDAP_USER_ATTR_MAP = {
    'first_name': 'givenName',
    'last_name': 'sn',
    'email': 'mail'
}

AUTH_LDAP_GROUP_TYPE = NestedActiveDirectoryGroupType()

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    "is_superuser": "cn=edms-conf,ou=Groups,dc=example,dc=org",
    "is_active": "cn=allstaff,ou=Groups,dc=example,dc=org",
    "is_staff": "cn=edms-conf,ou=Groups,dc=example,dc=org"
}

AUTH_LDAP_GROUP_SEARCH = LDAPSearchUnion(
    LDAPSearch("ou=Groups,dc=example,dc=org", ldap.SCOPE_SUBTREE, "(&(objectClass=group)(cn=IT))"),
    LDAPSearch("ou=Groups,dc=example,dc=org", ldap.SCOPE_SUBTREE, "(&(objectClass=group)(cn=allstaff))"),
    LDAPSearch("ou=Groups,dc=example,dc=org", ldap.SCOPE_SUBTREE, "(&(objectClass=group)(cn=edms-conf))"),
    LDAPSearch("ou=Mayan-EDMS,ou=Groups,dc=example,dc=org", ldap.SCOPE_SUBTREE, "(objectClass=group)"),
)

AUTH_LDAP_CACHE_GROUPS = True
AUTH_LDAP_FIND_GROUP_PERMS = True
AUTH_LDAP_MIRROR_GROUPS = True

AUTH_LDAP_GROUP_CACHE_TIMEOUT = 3600

AUTHENTICATION_BACKENDS = (
    'django_remote_auth_ldap.backend.RemoteUserLDAPBackend',
    'django_auth_ldap.backend.LDAPBackend',
)

DRAL_CHECK_DOMAIN = False
DRAL_HEADER = 'HTTP_X_REMOTE_USER'

#CSRF_FAILURE_VIEW = 'mayan.apps.custom.views.csrf_failure'
